# Cronograma geral para elaboração dos entregáveis dos pontos de controle

| Atividades                          | Responsável                            | Prazo    |
|-------------------------------------|----------------------------------------|----------|
| Problematização                     | Todos                                  | 16/09/23 |
| Organização da Equipe               | Todos                                  | 16/09/23 |
| Definição do Escopo e Requisitos    | Todos                                  | 23/09/23 |
| Análise de Viabilidade              | Diretores Técnicos                     | 23/09/23 |
| Revisão Bibliográfica               | Todos                                  | 23/09/23 |
| Termo de Abertura do Projeto        | Diretor de qualidade e desenvolvedores | 26/09/23 |
| Arquitetura Geral                   | Diretores Técnicos                     | 26/09/23 |
| Arquitetura de cada Subsistema      | Desenvolvedores                        | 28/09/23 |
| Planejamento e Cronograma           | Coordenador Geral                      | 28/09/23 |
| Levantamento de Riscos              | Desenvolvedores                        | 28/09/23 |
| Definição de Tecnologias            | Desenvolvedores                        | 07/10/23 |
| Compra de Materiais                 | Coordenador Geral e Diretores técnicos | 14/10/23 |
| Desenvolvimento dos Subsistemas     | Desenvolvedores                        | 21/10/23 |
| Cálculos e Modelagens               | Desenvolvedores                        | 21/10/23 |
| Plano de Integração dos Subsistemas | Diretores técnicos                     | 21/10/23 |
| Testes e Validação de Soluções      | Desenvolvedores                        | 28/10/23 |
| Integração de Subsistemas           | Desenvolvedores                        | 24/11/23 |
| Manual de Montagem e Uso            | Desenvolvedores                        | 24/11/23 |
| Manual de Manutenção                | Desenvolvedores                        | 24/11/23 |