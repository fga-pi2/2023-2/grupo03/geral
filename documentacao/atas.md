# Registro de reuniões 

## 22/09/23 - Geral 

- Saiu o cronograma inicial para o desenvolvimento dos documentos necessários para o PC1

- Todas as áreas alinharam seu papel dentro do projeto e foram levantadas algumas ideias e soluções para atingir o problema

- Documentos principais de gerenciamento serão disponibilizados na segunda (25/09)

- Reunião geral para apresentação dos documentos na quarta (27/09) e entrega da documentação de cada parte no dia (28/09)

## 27/09/23 - Geral

- Apresentação do desenvolvimento de atividades de cada setor.

- Geral: 
    - Foi apresentado o detalhamento do problema, objetivos gerais e discutido com todos os integrantes sobre os requisitos gerais

    - Na parte de software foram apresentados documentos que cobriam requisitos especificos, arquitetura geral, normas tecnicas, levantamento de custos, riscos e outros. Ainda estava em desenvolvimento a parte de EAP

    - Em eletrônica foram apresentados a arquitetura específica, a EAP. O levantamento das normas, custo e requisitos específicos ainda estavam em andamento.
    
    - Em estruturas foram apresentadas algumas soluções já em CAD, e a EAP de estruturas. Os requisitos específicos e documentos de arquitetura ainda estavam em desenvolvimento
   
    - Os membros responsáveis de energia não puderam participar da reunião porém, haviam concluido o a arquitetura do sistema e decidido que a EAP de energia iria fazer parte da eletrônica, integrando os dois subsistemas. Os requisitos específicos, normas e levantamento de custos ainda estavam sendo levantados. 

- Entrega das atividades para a quinta e revisão por parte dos líderes durante o fim de semana. Cada desenvolvedor foi atualizado quanto a ajustes necessários dos documentos feitos para integrar no relatório do PC1 da melhor forma possível

- Finalização e integração total irá acontecer no fim de semana e a entrega do PC1 será feita pelo coordenador geral no domingo a noite. 

## 04/10/23 - Geral 

- Apresentação de Sexta (06/10)
    - Vamos ver como está apresentação dos grupos de quarta e organizar entre nós. A ideia é ter pelo menos 1 de cada área e 2 de software.

- Definição de Tecnologias
    - Falta apenas o motor, precisamos resolver essa parte o quanto antes para não atrasar a compra de materiais 

- Compra de materiais 
    - Cada área deverá listar todos os gastos necessários (até o dia 14/10, se possível bem antes) para que sejam feitas as compras. 
    - Vamos fazer uma lista de tudo que conseguimos emprestado também
    - A ideia é realizar o reembolso ao final do projeto. Porém, caso o item ou serviço a ser adquirido seja muito mais caro do que o setor consiga bancar, podemos solicitar o reembolso imeditado por parte da equipe.
    - Portanto, cada setor ficará responsável por fazer o levantamento de materiais e serviços para compra 

- Desenvolvimento de subsistemas, calculos, modelagens e planos de integração. 
    - Dia 21 fica a entrega parcial da parte de modelagem e integração
    - Dia 28 fica a entrega completa do subsistema funcional.
    - Reuniões de acompanhamento de ambas as fases acontecerão nos dias 18, 20, 25 e 27 de outubro.