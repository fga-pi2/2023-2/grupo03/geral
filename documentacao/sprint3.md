## Sprint 3 (23/10 - 27/10)

### Retrospectiva

A equipe discutiu sobre as tarefas concluídas/não concluídas.

### Planning

Durante a reunião a equipe se subdividiu em subsistemas.O subsistema software foi separado em Backend, Frontend e Software Embarcado, com isso, as tarefas foram especificadas.

- **Backend**

Desenvolvimento de endpoints relacionados ao épico de satélites.

- **Frontend**

Implementação de telas relacionadas ao satélite com dados mockados e ajustes no protótipo sobre o apontamento.

- **Software embarcado**

Testes dos sensores e do Lora.
