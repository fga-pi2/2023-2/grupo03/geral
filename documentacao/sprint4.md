## Sprint 3 (30/10 - 03/11)

### Retrospectiva

Foi discutido o que foi concluído ou não e integrações entre áreas.

### Planning

Durante a reunião a equipe se subdividiu em subsistemas. O subsistema software foi separado em Backend, Frontend e Software Embarcado, com isso, as tarefas foram especificadas.

- **Backend**

Desenvolvimento de endpoints relacionados ao épico de satélites.

- **Frontend**

Integração com API e endpoints de CRUD e usuário e implementação de tela Home com dados mockados.

- **Software embarcado**

Integração entre sensores, comunicação Lora e movimentação do motor.
