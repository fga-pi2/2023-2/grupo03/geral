## Sprint 1 (09/10 - 13/10)

### Planning

Durante a reunião a equipe se subdividiu em subsistemas.O subsistema software foi separado em Backend, Frontend e Software Embarcado, com isso, as tarefas foram especificadas.

- **Backend**

Criação de endpoints create, read, update, delete

- **Frontend**

Criação de protótipo de alta fidelidade e inicialização do repositório


- **Software embarcado**

Definição final de sensores e pesquisa comercial para compra

