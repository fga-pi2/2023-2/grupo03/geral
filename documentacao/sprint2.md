## Sprint 2 (16/10 - 20/10)

### Retrospectiva

Os squads especificaram o que foi feito e o que não pode ser concluído. As equipes discutiram medidas para melhorar a produtividade.

### Planning

Durante a reunião a equipe se subdividiu em subsistemas.O subsistema software foi separado em Backend, Frontend e Software Embarcado, com isso, as tarefas foram especificadas.

- **Backend**

Endpoint patch para autorização de acesso do usuário e userRole, endpoint de login e logout e token.

- **Frontend**

CRUD de usuário com dados mockados e componentes a serem utilizados.

- **Software embarcado**

Teste do Lora.
