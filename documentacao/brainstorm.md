# Brainstorm

## Histórico de Versões

| Data | Versão | Modificação | Autor |
| :- | :- | :- | :- |
|26/09/2023 | 0.1 | Criação  e elaboração do documento | [Daniel Oda](https://gitlab.com/danieloda)

## Introdução

O Brainstorming é uma das mais conhecidas técnicas de Elicitação de requisitos. Ela é utilizada para estimular o surgimento de soluções e ideias criativas a partir da discussão entre integrantes da equipe. 

Nessa reunião, há uma explicação do problema, exposição das ideais dos participantes, discussão sobre elas e um agrupamento das mesmas

## Metodologia

Realizamos uma sessão de brainstorming com o objetivo de gerar e consolidar ideias relacionadas ao tema escolhido, durante a qual identificamos requisitos funcionais e requisitos não funcionais.

Decidimos nos encontrar remotamente através do Discord e utilizar a plataforma de colaboração digital "[Miro](miro.com)". Foi dado um tempo para que os integrantes do grupos listassem suas ideias e após isso o grupo passou de ideia em ideia discutindo sua viabilidade e prioridade dentro do projeto.

<figure>
  <a href="https://freeimage.host/br"><img src="https://iili.io/JJPkhFf.png" alt="JJPkhFf.png" border="0" /></a>
  <figcaption>Figura 1 - Brainstorm. Fonte: Autores </figcaption>
</figure>

## Resultado

A partir da sessão de brainstorming, conseguimos elicitar os seguintes requisitos:

| ID | Descrição | Tipo de requisito |
|:-: | :- | :-: |
| BS01 | O usuário deve ser capaz de cadastrar uma antena| RF |
| BS02 | O usuário deve ser capaz de buscar satélites| RF |
| BS03 | O usuário deve ser capaz de listar o histórico de satélites que foram conectados| RF |
| BS04 | O usuário deve ser capaz de favoritar satélites| RF |
| BS05 | O usuário deve ser capaz de visualizar página do satélite com todas as suas informações| RF |
| BS06 | O usuário deve ser capaz de visualizar dashboard com informações relevantes da conexão atual com o satélite | RF |
| BS07 | O usuário deve ser capaz de se cadastrar no sistema| RF |
| BS08 | O sistema deve ser capaz de calcular e enviar dados para a movimentação da antena | RF |
| BS09 | O sistema deverá requisitar dados de posição do satélite para uma API externa | RF |
| BS10 | O sistema deverá ser web | RNF |

#### Legenda

| Legenda | Significado |
|:-: | :- |
|BS|Brainstorm|
|RF|Requisito Funcional|
|RNF|Requisito Não Funcional|

## Conclusão

Na conclusão da sessão de brainstorming, conseguimos compilar uma lista abrangente de requisitos, abrangendo tanto os aspectos funcionais quanto os não funcionais, fornecendo assim uma base para a próxima fase do nosso projeto. 