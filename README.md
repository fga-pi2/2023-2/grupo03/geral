# Grupo 03 – Controle de apontamento de antenas

Repositório do projeto Controle de Apontamento de Antenas da disciplina Projeto Integrador II - UnB (FGA)
## Organograma

|Nome|Matrícula|Cargo|
|--|--|--|
|**Gustavo Henrique Rodrigues Viana de Oliveira**|190014091|Coordenador Geral e Desenvolvedor de Eletrônica|
|**Paolla Leticia Cardoso Quaresma**|190047917|Diretora de Qualidade e Desenvolvedora de Estruturas|
|**Vinicius Barbosa Maciel Couto**|190048620|Diretor Técnico de Estruturas|
|Kevin Tales|22252155|Desenvolvedor de Estruturas|
|Pedro Henrique Braga|180036637|Desenvolvedor de Estruturas|
|**Vitória Bandeira Melo**||Diretora Técnica de Eletrônica|
|Pedro Torres||Diretor Técnico de Energia|
|Leo Orlando||Desenvolvedor de Energia|
|**Dafne Moretti**||Diretora Técnica de Software|
|Daniel Oda||Desenvolvedor de Software|
|Lucas Gabriel Bezerra||Desenvolvedor de Software|
|João Vitor Lopes de Farias||Desenvolvedor de Software|
|Gustave Persijn||Desenvolvedor de Software|
|Igor Queiroz Lima||Desenvolvedor de Software|
|Lucas Andrade||Desenvolvedor de Software|
|João Pedro Alves da Silva Chaves||Desenvolvedor de Software|
|Eduarda Rodrigues Tavares||Desenvolvedor de Software|
|Ricardo de Castro Loureiro||Desenvolvedor de Software|
